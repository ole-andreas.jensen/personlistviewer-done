package no.uib.inf101.sample.view;

import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.uib.inf101.sample.eventbus.EventBus;

/**
* The panel for creating a person. Contains fields for name and age,
* and a button for creating the person. When the button is clicked, a
* PersonCreatedEvent is posted to the event bus.
*/
public class ViewCreatePerson {
  
  private JPanel mainPanel;
  
  /** 
  * Create a new view for creating a person.
  * @param eventBus The event bus to post events to
  */
  public ViewCreatePerson(final EventBus eventBus) {
    this.mainPanel = new JPanel();
    this.mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
    this.mainPanel.add(new JLabel("Name:"));
    JTextField nameField = new JTextField();
    this.mainPanel.add(nameField);
    this.mainPanel.add(new JLabel("Age:"));
    JTextField ageField = new JTextField();
    this.mainPanel.add(ageField);
    JButton createButton = new JButton("Create");
    this.mainPanel.add(createButton);
    
    ActionListener listener = (e) -> {
      String name = nameField.getText();
      int age = Integer.parseInt(ageField.getText());
      eventBus.post(new PersonCreatedEvent(name, age));
    };
    createButton.addActionListener(listener);
  }
  
  /** Get the main panel for this component */
  public JPanel getMainPanel() {
    return this.mainPanel;
  }
}
