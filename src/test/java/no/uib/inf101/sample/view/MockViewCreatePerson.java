package no.uib.inf101.sample.view;

import javax.swing.JFrame;

import no.uib.inf101.sample.eventbus.EventBus;

public class MockViewCreatePerson {
  
  public static void main(String[] args) {
    EventBus eventBus = new EventBus();
    ViewCreatePerson view = new ViewCreatePerson(eventBus);
    eventBus.register(e -> {
      if (e instanceof PersonCreatedEvent event) {
        System.out.println(event);
      }
    });
    
    JFrame frame = new JFrame("MOCK");
    frame.setContentPane(view.getMainPanel());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
  
}
