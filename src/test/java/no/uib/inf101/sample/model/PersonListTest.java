package no.uib.inf101.sample.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class PersonListTest {
  
  @Test
  public void sanityTest() {
    PersonList model = new PersonList();
    model.addPerson(new Person("Adam", 20));
    model.addPerson(new Person("Eva", 19));
    
    List<Person> resultOfIteration = new ArrayList<>();
    for (Person person : model.getPersons()) {
      resultOfIteration.add(person);
    }
    
    assertEquals(2, resultOfIteration.size());
    assertTrue(resultOfIteration.contains(new Person("Adam", 20)));
    assertTrue(resultOfIteration.contains(new Person("Eva", 19)));
  }
}
